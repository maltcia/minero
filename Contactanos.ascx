<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>

<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />

<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssContactanos" FilePath="css/contactanos.css" PathNameAlias="SkinPath" />

<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssClassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />

<!-- JS Includes -->
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />

<dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />

<body>
	<div id="ContentPane" runat="server"></div>
	<section id="baner" class="section-baner">
      <div class="container-fluid">
        <div class="row">
          <div class="col col-12 col-lg-4 bg-fern content-title d-flex justify-content-center align-items-center"  >
            
            <img src="<%=SkinPath %>images/adorno-baner.png" srcset="<%=SkinPath %>images/adorno-baner.svg" class="adorno">
            <h1 class="title text-white d-inline-block librefranklin-thin" data-aos="fade-right">
              Contáctenos
              <span class="d-block text-white librefranklin-bold  " data-aos="fade-up-right">
                Tienes dudas<br>
                contáctanos
              </span>
            </h1>

          </div>
          <div class="col col-12 col-lg-8 bg-img" data-aos="fade-left" style="background-image: url('<%=SkinPath %>images/banner.png');">
          </div>
          <div class="line-color">

          </div>
          <div class="col col-12 bg-pumice nav">
            <div class="container d-flex align-items-center flex-wrap">
              <a href="#" class="nav-item item1 librefranklin-thin color-nevada color-hover-nevada d-inline-flex align-items-center">
                <img src="<%=SkinPath %>images/home.png" srcset="<%=SkinPath %>images/home.svg" class="icon">
                Inicio
              </a>
              <a href="#" class="nav-item librefranklin-thin color-nevada color-hover-nevada d-inline-flex align-items-center">
                Contáctenos
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="contact" class="section-contact">
      <div class="container-fluid section-contact-row">
         <div class="row">
          <div class="col-sm-12 col-lg-6 section-contact-col-left" data-aos="fade-right">
              <p class="contact-paragraph-one librefranklin-regular color-very-dark-gray" >Para nosotros son muy importantes sus comentarios;
                 si desea comunicarse con nosotros,</p>
               <span class="contact-paragraph-two librefranklin-bold color-very-dark-gray">déjanos tu información. </span>
              <div class="img-woman">
                  <img src="<%=SkinPath %>images/woman.png" class="">
              </div>
          </div>
          <div class="col-sm-12 col-lg-6 section-contact-col-right">
                <div class="form-frame bg-very-light-gray" data-aos="fade-left">
                  <div class="form librefranklin-regular color-very-dark-gray bg-very-light-gray">
                        <div class="row">
                              <div class="col-12">
                                <div class="form-group">
                                    <label for="nombre" class="control-label sr-only">Nombre completo*</label>
                                    <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre completo *" required>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="mail" class="control-label sr-only">E-mail</label>
                                    <input type="email" name="mail" id="mail" class="form-control" placeholder="E-mail *" required>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label for="telefono" class="control-label sr-only">Teléfono</label>
                                    <input type="tel" name="telefono" id="telefono" class="form-control" placeholder="Teléfono">
                                </div>
                            </div>
                             <div class="col-12 col-sm-6">
                               <div class="form-group">
                                    <label for="ciudad" class="control-label sr-only">Ciudad</label>
                                    <input type="text" name="ciudad" id="ciudad" class="form-control" placeholder="Ciudad *" required>
                                </div>
                              </div>
                            <div class="col-12 ">
                              <div class="form-group librefranklin-regular color-very-dark-gray">
                                 <label for="asunto" class="control-label sr-only">Seleccione asunto</label>
                                    <select class="form-control custom-select" id="asunto" name="asunto">
                                       <option> --- Seleccione un asunto ---</option>
                                       <option></option>
                                       <option></option>
                                       <option></option>
                                       <option></option>
                                    </select>
                              </div>
                             </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="mensaje" class="control-label sr-only">Mensaje</label>
                                    <textarea name="mensaje" id="mensaje" rows="3" placeholder="Mensaje" class="form-control"></textarea>
                                </div>
                                <div class="col-12 form-footer bg-fern text-white text-center" data-aos="fade-up-left">
                                    <p class="text-center">Tenga en cuenta que al utilizar sus datos personales para
                                       contactarnos, autoriza y acepta las políticas de Habeas Data
                                       de Mineros S.A.  </p>
                                </div>
                            </div>
                      </div>
                  </div>
                </div>
            </div>
         </div>
      </div>
    </section>
</body>