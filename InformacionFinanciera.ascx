<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>

<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />

<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssInternaInforFinanciera" FilePath="css/informacion-financiera.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssAos" FilePath="css/aos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssClassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />

<!-- JS Includes -->
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsAos" FilePath="js/aos.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />


<body>
	<div id="ContentPane" runat="server"></div>
    <section id="baner" class="section-baner">
        <div class="container-fluid">
            <div class="row min-height" data-aos="fade-left">
                <div class="col col-12 col-lg-4 col-md-6 bg-fern content-title d-flex justify-content-end align-items-center px-sm-0">
                    <img src="<%=SkinPath %>images/adorno-baner.png" srcset="<%=SkinPath %>images/adorno-baner.svg" class="adorno">
                    <!--<h1 class="title text-white d-inline-block librefranklin-thin">
                        Nosotros
                        <span class="d-block text-white librefranklin-bold">Quienes somos</span>
                    </h1>-->
                    <div class="bg-white caja position-relative" data-aos="fade-up">
                        <span class="line-color position-absolute top-0 left-0"></span>
                        <h1 class="librefranklin-thin color-scorpion" data-aos="fade-right">Inversionistas</h1>
                        <p class="mb-0 librefranklin-bold text-capitalize color-scorpion" data-aos="fade-left">Información Financiera</p>
                        <span class="line d-block bg-corn"></span>
                    </div>
                </div>
                <!--.col-12-->
                <div class="col col-12 col-lg-8 col-md-6 bg-img px-sm-0">
                </div>
                <div class="line-color">
                </div>
            </div>
        </div>
    </section>
    <!--#baner-->
    <section class="section-navigation bg-pumice" data-aos="fade-left">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-pumice">
                    <li class="breadcrumb-item" data-aos="fade-right">
                        <a href="#" class="librefranklin-thin color-nevada color-hover-nevada d-flex align-items-center">
                            <img src="<%=SkinPath %>images/home.png" srcset="<%=SkinPath %>images/home.svg" class="icon"> Inicio
                        </a>
                    </li>
                    <li class="breadcrumb-item librefranklin-thin color-nevada active" aria-current="page" data-aos="fade-right">Información Financiera</li>
                </ol>
            </nav>
        </div>
        <!--.container-->
    </section>
    <!--.navigation-->
    <section class="section-informacion" id="informacion">
        <div class="container px-sm-0">
            <div class="row m-0">
                <div class="col-12 col-sm-12 col-md-12 col-lg-9 column-content px-md-0">
                    <div class="row m-0 dividendos">
                        <div class="col-12 px-0">
                            <div class="title position-relative bg-jungle-green" data-aos="fade-up-left">
                                <h3 class="h4 text-white font-weight-bold librefranklin-bold" data-aos="fade-left">Información <br /> sobre pago de dividendos</h3>
                                <span class="line-vertical position-absolute top-0 bg-corn"></span>
                            </div>
                            <!--.title-->
                        </div>
                        <!--.col-12-->
                        <div class="col-12 paragraph-content">
                            <p class="paragraph bg-alto color-scorpion librefranklin-regular" data-aos="fade-left">Según la ley 1819 de 2016 están sujetos a retención en la fuente los dividendos pagados a personas naturales cuando superan 600 UVT ($19.893.600). Esta retención está establecida en el artículo 242 del Estatuto Tributario mediante tabla progresiva que estipula el cálculo dependiendo el monto. Esta retención la certificaremos y podrá tomarla como mecanismo e pago de su declaración de renta año gravable 2018.</p>
                            <p class="paragraph bg-alto color-scorpion librefranklin-regular" data-aos="fade-right">Para personas y sociedades no residentes en Colombia la retención es del 5% del valor total del dividendo pagado.</p>
                        </div>
                        <!--.col-12-->
                    </div>
                    <!--.row-->
                    <div class="row m-0 participacion">
                        <div class="col-12 title px-0 d-flex">
                            <div class="box-icon bg-jungle-green d-flex justify-content-center align-items-center" data-aos="flip-left">
                                <img src="<%=SkinPath %>images/icon-char.png" srcset="<%=SkinPath %>images/icon-char.svg" align="Icon Chart" class="img-fluid" width="42" height="42">
                            </div>
                            <h3 class="h4 librefranklin-bold text-white bg-corn mb-0 w-100" data-aos="fade-left">Participación accionaria</h3>
                        </div>
                        <!--.title-->
                        <div class="col-12 px-0 m-negativo" data-aos="fade-up-right">
                            <ul class="list-group">
                                <li class="list-group-item d-flex rounded-0 align-items-end">
                                    <span class="icon-pdf" data-aos="flip-left">
                                        <img src="<%=SkinPath %>images/icon-pdf.png" srcset="<%=SkinPath %>images/icon-pdf.svg" class="img-fluid" align="Pdf icon">
                                        <!-- <i class="far fa-file-pdf" style="font-size: 48px;"></i> -->
                                    </span>
                                    <p class=" texto librefranklin-regular color-scorpion text-capitalize" data-aos="fade-left">Participación Accionaria 2018</p>
                                    <a href="#" class="icon-donwload ml-auto" data-aos="flip-right">
                                        <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" class="img-fluid" align="Download Icon">
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 px-0 d-flex flex-column flex-sm-row">
                            <div class="card box-link w-50 rounded-0 ml-0 border-color-alto" data-aos="fade-up-left">
                                <div class="card-body text-center pb-0">
                                    <h4 class="h4 card-title librefranklin-bold color-scorpion" data-aos="fade-left">Superintendencia <br /> Financiera de Colombia</h4>
                                </div>
                                <div class="card-footer bg-transparent border-0 p-0">
                                    <a href="#" class="btn bg-feijoa librefranklin-bold text-white d-flex justify-content-center align-items-center" data-aos="fade-left">
                                        <img src="<%=SkinPath %>images/icon-hyperlink.png" srcset="<%=SkinPath %>images/icon-hyperlink.svg" class="img-fluid icon-hyperlink" alt="Icon hyperlink" data-aos="flip-left">Ir al sitio
                                    </a>
                                </div><!--.card-footer-->
                            </div>
                            <!--.box-link-->
                            <div class="card box-link w-50 rounded-0 mr-0 border-color-alto" data-aos="fade-up-right">
                                <div class="card-body text-center pb-0">
                                    <h4 class="h4 card-title librefranklin-bold color-scorpion" data-aos="fade-right">Bolsa de Valores <br /> de Colombia</h4>
                                </div>
                                <div class="card-footer bg-transparent border-0 p-0">
                                    <a href="#" class="btn bg-feijoa librefranklin-bold text-white d-flex justify-content-center align-items-center" data-aos="fade-right">
                                        <img src="<%=SkinPath %>images/icon-hyperlink.png" srcset="<%=SkinPath %>images/icon-hyperlink.svg" class="img-fluid icon-hyperlink" alt="Icon hyperlink" data-aos="flip-right">Ir al sitio
                                    </a>
                                </div><!--.card-footer-->
                            </div>
                            <!--.box-link-->
                        </div>
                        <!--.col-12-->
                    </div>
                    <!--.participacion-->
                    <div class="row m-0 presentacion">
                        <div class="col-12 title px-0 d-flex">
                            <div class="box-icon bg-jungle-green d-flex justify-content-center align-items-center" data-aos="flip-left">
                                <img src="<%=SkinPath %>images/icon-list.png" srcset="<%=SkinPath %>images/icon-list.svg" align="Icon List" class="img-fluid" width="42" height="42">
                            </div>
                            <h3 class="h4 librefranklin-bold text-white bg-corn mb-0 w-100" data-aos="fade-left">Presentación de resultados</h3>
                        </div>
                        <div class="col-12 px-0">
                            <div class="collapse-conatiner">
                                <a class="btn btn-link btn-collpase rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-aos="fade-left">
                                    <span class="icon-collapse icon-rotateZ"><img src="<%=SkinPath %>images/arrow-circle-top.png" width="27.068" height="27.07" data-aos="flip-left"></span><span class="text librefranklin-bold color-mineral-green">2018</span>
                                </a>
                                <div class="collapse" id="collapseExample">
                                    <div class="card card-body border-color-alto rounded-0">
                                        <div class="collapse-trimestres">
                                            <a class="btn btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExampleInterno1" role="button" aria-expanded="false" aria-controls="collapseExampleInterno1">
                                                <span class="icon-collapseExampleInterno"><img src="<%=SkinPath %>images/icon-user-2.png" width="24.177" height="26.809"></span>
                                                <span class="text librefranklin-bold color-scorpion">III TRIMESTRE</span>
                                            </a>
                                            <div class="collapse" id="collapseExampleInterno1">
                                                <div class="card card-body rounded-0 border-color-alto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Webcast presentación de resultados III trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Resultados financieros III trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                    </ul>
                                                    <!--.list-group-->
                                                </div>
                                                <!--.card-->
                                            </div>
                                            <!--.collapse-->
                                        </div>
                                        <!--.collapse-trimestres-->
                                        <div class="collapse-trimestres">
                                            <a class="btn btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExampleInterno2" role="button" aria-expanded="false" aria-controls="collapseExampleInterno2">
                                                <span class="icon-collapseExampleInterno"><img src="<%=SkinPath %>images/icon-user-2.png" width="24.177" height="26.809"></span>
                                                <span class="text librefranklin-bold color-scorpion">II TRIMESTRE</span>
                                            </a>
                                            <div class="collapse" id="collapseExampleInterno2">
                                                <div class="card card-body rounded-0 border-color-alto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Webcast presentación de resultados II trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Resultados financieros II trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                    </ul>
                                                    <!--.list-group-->
                                                </div>
                                                <!--.card-->
                                            </div>
                                            <!--.collapse-->
                                        </div>
                                        <!--.collapse-trimestres-->
                                        <div class="collapse-trimestres">
                                            <a class="btn btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExampleInterno3" role="button" aria-expanded="false" aria-controls="collapseExampleInterno3">
                                                <span class="icon-collapseExampleInterno"><img src="<%=SkinPath %>images/icon-user-2.png" width="24.177" height="26.809"></span>
                                                <span class="text librefranklin-bold color-scorpion">I TRIMESTRE</span>
                                            </a>
                                            <div class="collapse" id="collapseExampleInterno3">
                                                <div class="card card-body rounded-0 border-color-alto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Webcast presentación de resultados I trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Resultados financieros I trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                    </ul>
                                                    <!--.list-group-->
                                                </div>
                                                <!--.card-->
                                            </div>
                                            <!--.collapse-->
                                        </div>
                                        <!--.collapse-trimestres-->
                                    </div>
                                    <!--.card-->
                                </div>
                            </div>
                            <!--.collapse-conatiner-->
                            <div class="collapse-conatiner">
                                <a class="btn btn-collpase btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample" data-aos="fade-right">
                                    <span class="icon-collapse icon-rotateZ"><img src="<%=SkinPath %>images/arrow-circle-top.png" width="27.068" height="27.07" data-aos="flip-left"></span><span class="text librefranklin-bold color-mineral-green">2017</span>
                                </a>
                                <div class="collapse" id="collapseExample2">
                                    <div class="card card-body border-color-alto rounded-0">
                                        <div class="collapse-trimestres">
                                            <a class="btn btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExampleInterno1" role="button" aria-expanded="false" aria-controls="collapseExampleInterno1" data-aos="fade-left">
                                                <span class="icon-collapseExampleInterno" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-user-2.png" width="24.177" height="26.809"></span>
                                                <span class="text librefranklin-bold color-scorpion">III TRIMESTRE</span>
                                            </a>
                                            <div class="collapse" id="collapseExampleInterno1">
                                                <div class="card card-body rounded-0 border-color-alto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Webcast presentación de resultados III trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Resultados financieros III trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                    </ul>
                                                    <!--.list-group-->
                                                </div>
                                                <!--.card-->
                                            </div>
                                            <!--.collapse-->
                                        </div>
                                        <!--.collapse-trimestres-->
                                        <div class="collapse-trimestres">
                                            <a class="btn btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExampleInterno2" role="button" aria-expanded="false" aria-controls="collapseExampleInterno2" data-aos="fade-right">
                                                <span class="icon-collapseExampleInterno" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-user-2.png" width="24.177" height="26.809"></span>
                                                <span class="text librefranklin-bold color-scorpion">II TRIMESTRE</span>
                                            </a>
                                            <div class="collapse" id="collapseExampleInterno2">
                                                <div class="card card-body rounded-0 border-color-alto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Webcast presentación de resultados II trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Resultados financieros II trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                    </ul>
                                                    <!--.list-group-->
                                                </div>
                                                <!--.card-->
                                            </div>
                                            <!--.collapse-->
                                        </div>
                                        <!--.collapse-trimestres-->
                                        <div class="collapse-trimestres">
                                            <a class="btn btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExampleInterno3" role="button" aria-expanded="false" aria-controls="collapseExampleInterno3"  data-aos="fade-left">
                                                <span class="icon-collapseExampleInterno" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-user-2.png" width="24.177" height="26.809"></span>
                                                <span class="text librefranklin-bold color-scorpion">I TRIMESTRE</span>
                                            </a>
                                            <div class="collapse" id="collapseExampleInterno3">
                                                <div class="card card-body rounded-0 border-color-alto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Webcast presentación de resultados I trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Resultados financieros I trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                    </ul>
                                                    <!--.list-group-->
                                                </div>
                                                <!--.card-->
                                            </div>
                                            <!--.collapse-->
                                        </div>
                                        <!--.collapse-trimestres-->
                                    </div>
                                    <!--.card-->
                                </div>
                            </div>
                            <!--.collapse-conatiner-->
                        </div>
                        <!--.title-->
                    </div>
                    <!--.presentacion-->
                    <div class="row m-0 consolidados">
                        <div class="col-12 title px-0 d-flex">
                            <div class="box-icon bg-jungle-green d-flex justify-content-center align-items-center" data-aos="flip-left">
                                <img src="<%=SkinPath %>images/icon-list.png" srcset="<%=SkinPath %>images/icon-list.svg" align="Icon List" class="img-fluid" width="42" height="42">
                            </div>
                            <h3 class="h4 librefranklin-bold text-white bg-corn mb-0 w-100" data-aos="fade-left">Informes Financieros Consolidados</h3>
                        </div>
                        <!--.title-->
                        <div class="col-12 px-0 d-flex m-negativo" data-aos="fade-up-left">
                            <div class="card w-100 rounded-0">
                                <div class="card-body">
                                    <ul class="list-group">
                                        <li class="list-group-item rounded-0" data-aos="fade-right">
                                            <a href="#" class="d-flex w-100 text-decoration-none align-items-center">
                                                <span style="margin-right: 20px;" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" class="img-fluid" align="Pdf Icon" width="24" height="30"></span></span>
                                                <p class="mb-0 librefranklin-regular color-scorpion" data-aos="fade-left">2017</p>
                                                <span class="ml-auto"><img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" class="img-fluid" align="Download Icon" width="16" height="16"></span>
                                            </a>
                                        </li>
                                        <li class="list-group-item rounded-0" data-aos="fade-left">
                                            <a href="#" class="d-flex w-100 text-decoration-none align-items-center">
                                                <span style="margin-right: 20px;" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" class="img-fluid" align="Pdf Icon" width="24" height="30"></span></span>
                                                <p class="mb-0 librefranklin-regular color-scorpion" data-aos="fade-left">2016</p>
                                                <span class="ml-auto"><img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" class="img-fluid" align="Download Icon" width="16" height="16"></span>
                                            </a>
                                        </li>
                                        <li class="list-group-item rounded-0" data-aos="fade-right">
                                            <a href="#" class="d-flex w-100 text-decoration-none align-items-center">
                                                <span style="margin-right: 20px;" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" class="img-fluid" align="Pdf Icon" width="24" height="30"></span></span>
                                                <p class="mb-0 librefranklin-regular color-scorpion" data-aos="fade-left">2015</p>
                                                <span class="ml-auto"><img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" class="img-fluid" align="Download Icon" width="16" height="16"></span>
                                            </a>
                                        </li>
                                        <li class="list-group-item rounded-0" data-aos="fade-left">
                                            <a href="#" class="d-flex w-100 text-decoration-none align-items-center">
                                                <span style="margin-right: 20px;" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" class="img-fluid" align="Pdf Icon" width="24" height="30"></span></span>
                                                <p class="mb-0 librefranklin-regular color-scorpion" data-aos="fade-left">2014</p>
                                                <span class="ml-auto"><img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" class="img-fluid" align="Download Icon" width="16" height="16"></span>
                                            </a>
                                        </li>
                                        <li class="list-group-item rounded-0" data-aos="fade-right">
                                            <a href="#" class="d-flex w-100 text-decoration-none align-items-center">
                                                <span style="margin-right: 20px;" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" class="img-fluid" align="Pdf Icon" width="24" height="30"></span></span>
                                                <p class="mb-0 librefranklin-regular color-scorpion" data-aos="fade-left">2013</p>
                                                <span class="ml-auto"><img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" class="img-fluid" align="Download Icon" width="16" height="16"></span>
                                            </a>
                                        </li>
                                        <li class="list-group-item rounded-0" data-aos="fade-left">
                                            <a href="#" class="d-flex w-100 text-decoration-none align-items-center">
                                                <span style="margin-right: 20px;" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" class="img-fluid" align="Pdf Icon" width="24" height="30"></span></span>
                                                <p class="mb-0 librefranklin-regular color-scorpion" data-aos="fade-left">2012</p>
                                                <span class="ml-auto"><img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" class="img-fluid" align="Download Icon" width="16" height="16"></span>
                                            </a>
                                        </li>
                                        <li class="list-group-item rounded-0" data-aos="fade-right">
                                            <a href="#" class="d-flex w-100 text-decoration-none align-items-center">
                                                <span style="margin-right: 20px;" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" class="img-fluid" align="Pdf Icon" width="24" height="30"></span></span>
                                                <p class="mb-0 librefranklin-regular color-scorpion" data-aos="fade-left">2011</p>
                                                <span class="ml-auto"><img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" class="img-fluid" align="Download Icon" width="16" height="16"></span>
                                            </a>
                                        </li>
                                        <li class="list-group-item rounded-0" data-aos="fade-left">
                                            <a href="#" class="d-flex w-100 text-decoration-none align-items-center">
                                                <span style="margin-right: 20px;" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" class="img-fluid" align="Pdf Icon" width="24" height="30"></span></span>
                                                <p class="mb-0 librefranklin-regular color-scorpion" data-aos="fade-left">2010</p>
                                                <span class="ml-auto"><img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" class="img-fluid" align="Download Icon" width="16" height="16"></span>
                                            </a>
                                        </li>
                                        <li class="list-group-item rounded-0" data-aos="fade-right">
                                            <a href="#" class="d-flex w-100 text-decoration-none align-items-center">
                                                <span style="margin-right: 20px;" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" class="img-fluid" align="Pdf Icon" width="24" height="30"></span></span>
                                                <p class="mb-0 librefranklin-regular color-scorpion" data-aos="fade-left">2009</p>
                                                <span class="ml-auto"><img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" class="img-fluid" align="Download Icon" width="16" height="16"></span>
                                            </a>
                                        </li>
                                        <li class="list-group-item rounded-0" data-aos="fade-left">
                                            <a href="#" class="d-flex w-100 text-decoration-none align-items-center">
                                                <span style="margin-right: 20px;" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" class="img-fluid" align="Pdf Icon" width="24" height="30"></span></span>
                                                <p class="mb-0 librefranklin-regular color-scorpion" data-aos="fade-left">2008</p>
                                                <span class="ml-auto"><img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" class="img-fluid" align="Download Icon" width="16" height="16"></span>
                                            </a>
                                        </li>
                                        <li class="list-group-item rounded-0" data-aos="fade-right">
                                            <a href="#" class="d-flex w-100 text-decoration-none align-items-center">
                                                <span style="margin-right: 20px;" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" class="img-fluid" align="Pdf Icon" width="24" height="30"></span></span>
                                                <p class="mb-0 librefranklin-regular color-scorpion" data-aos="fade-left">2007</p>
                                                <span class="ml-auto"><img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" class="img-fluid" align="Download Icon" width="16" height="16"></span>
                                            </a>
                                        </li>
                                    </ul>
                                    <!--.list-group-->
                                </div>
                                <!--.card-body-->
                            </div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                    </div>
                    <!--.consolidados-->
                    <div class="row m-0 relacion">
                        <div class="col-12 px-0">
                            <div class="card bg-jungle-green border-0 rounded-0" data-aos="fade-up-left">
                                <div class="card-body">
                                    <h3 class="card-title librefranklin-bold text-white mb-0" data-aos="fade-right">Relación con Inversionistas</h3>
                                    <div class="list-group">
                                        <div class="list-group-item bg-transparent border-0 rounded-0">
                                            <span class="icon" style="margin-right: 15px;" data-aos="flip-left">
                                                <img src="<%=SkinPath %>images/icon-user.png" srcset="<%=SkinPath %>images/icon-user.svg" alt="Icon User" class="img-fluid" width="14.177" height="15.72">
                                            </span>
                                            <a href="#" class="text-decoration-none librefranklin-regular text-white" data-aos="fade-left">Paul Harris – Director Relación con Inversionistas.</a>
                                        </div>
                                        <!--.list-group-item-->
                                        <div class="list-group-item bg-transparent border-0 rounded-0">
                                            <span class="icon" style="margin-right: 15px;" data-aos="flip-left">
                                                <img src="<%=SkinPath %>images/icon-phone.png" srcset="<%=SkinPath %>images/icon-phone.svg" alt="Icon Phone" class="img-fluid" width="18.024" height="17.114">
                                            </span>
                                            <a href="#" class="text-decoration-none librefranklin-regular text-white" data-aos="fade-left">Teléfono: +(574) 2665757</a>
                                        </div>
                                        <!--.list-group-item-->
                                        <div class="list-group-item bg-transparent border-0 rounded-0">
                                            <span class="icon" style="margin-right: 15px;" data-aos="flip-left">
                                                <img src="<%=SkinPath %>images/icon-mail.png" srcset="<%=SkinPath %>images/icon-mail.svg" alt="Icon Mail" class="img-fluid" width="17.818" height="14">
                                            </span>
                                            <a href="#" class="text-decoration-none librefranklin-regular text-white" data-aos="fade-left">relacion.inversionistas@mineros.com.co</a>
                                        </div>
                                        <!--.list-group-item-->
                                        <div class="list-group-item bg-transparent border-0 rounded-0">
                                            <span class="icon" style="margin-right: 15px;" data-aos="flip-left">
                                                <img src="<%=SkinPath %>images/icon-mark.png.png" srcset="<%=SkinPath %>images/icon-mark.svg" alt="Icon mark" class="img-fluid" width="13.705" height="18.036">
                                            </span>
                                            <a href="#" class="text-decoration-none librefranklin-regular text-white" data-aos="fade-left">Medellín, Colombia</a>
                                        </div>
                                        <!--.list-group-item-->
                                    </div>
                                    <!--.list-group-->
                                </div>
                                <!--.card-body-->
                            </div>
                            <!--.card-->
                        </div>
                    </div>
                    <!--.relacion-->
                </div>
                <!--.column-content-->
                <div class="col-12 col-lg-3 aside pr-0">
                    <div class="row m-0 indicadores">
                        <div class="col-12 d-flex align-items-end pr-0 mb-3">
                            <div class="indicadores-ico bg-corn d-flex justify-content-center align-items-center" data-aos="flip-left">
                                <img src="<%=SkinPath %>images/indicadores-ico.png" srcset="<%=SkinPath %>images/indicadores-ico.svg" class="img-fluid" alt="Indicadores image">
                            </div>
                            <h3 class="h4 title mb-0 w-100 border-color-alto librefranklin-bold color-scorpion" data-aos="fade-left">Indicadores</h3>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-12 pr-0">
                            <div class="card bg-feijoa border-0 rounded-0" data-aos="fade-right">
                                <span class="span-simbolo position-absolute">$</span>
                                <div class="card-header border-0 rounded-0 bg-transparent position-relative">
                                    <h3 class="h4 card-title text-white librefranklin-regular text-uppercase" data-aos="zoon-in">ACCIÓN <span class="d-block librefranklin-bold">MINEROS</span></h3>
                                    <p class="card-text text-white librefranklin-bold" data-aos="zoon-in-up">1.920,00 <span class="moneda">COP</span></p>
                                </div>
                                <!--.card-header-->
                                <div class="card-body">
                                    <p class="card-text librefranklin-regular color-mineral-green" data-aos="zoon-in-left">Última marcación <span class="d-block librefranklin-bold color-mineral-green">04-03-2019 06:08 PM</span></p>
                                </div>
                                <!--.card-body-->
                            </div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 pr-0">
                            <div class="card bg-fern border-0 rounded-0" data-aos="fade-left">
                                <span class="span-simbolo position-absolute">$</span>
                                <div class="card-header border-0 rounded-0 bg-transparent position-relative">
                                    <h3 class="h4 card-title text-white librefranklin-regular text-uppercase" data-aos="zoon-in">Valor <span class="d-block librefranklin-bold">de Oro</span></h3>
                                    <p class="card-text text-white librefranklin-bold" data-aos="zoon-in-up">1.286,06 <span class="moneda">USD/Oz</span></p>
                                </div>
                                <!--.card-header-->
                                <div class="card-body">
                                    <p class="card-text librefranklin-regular color-mineral-green" data-aos="zoon-in-left">Última marcación <span class="d-block librefranklin-bold color-mineral-green">04-03-2019 01:10 PM</span></p>
                                </div>
                                <!--.card-body-->
                            </div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 pr-0">
                            <div class="card bg-jungle-green border-0 rounded-0" data-aos="fade-right">
                                <span class="span-simbolo position-absolute">$</span>
                                <div class="card-header border-0 rounded-0 bg-transparent position-relative">
                                    <h3 class="h4 card-title text-white librefranklin-regular text-uppercase" data-aos="zoon-in">Valor <span class="d-block librefranklin-bold">de la plata</span></h3>
                                    <p class="card-text text-white librefranklin-bold" data-aos="zoon-in-up">15,22 <span class="moneda">USD/Oz</span></p>
                                </div>
                                <!--.card-header-->
                                <div class="card-body">
                                    <p class="card-text librefranklin-regular color-mineral-green" data-aos="zoon-in-left">Última marcación <span class="d-block librefranklin-bold color-mineral-green">04-03-2019 08:00 P M</span></p>
                                </div>
                                <!--.card-body-->
                            </div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 pr-0">
                            <div class="card bg-mineral-green border-0 rounded-0" data-aos="fade-left">
                                <span class="span-simbolo position-absolute">$</span>
                                <div class="card-header border-0 rounded-0 bg-transparent position-relative">
                                    <h3 class="h4 card-title text-white librefranklin-regular text-uppercase" data-aos="zoon-in"><span class="d-block librefranklin-bold">TRM</span></h3>
                                    <p class="card-text text-white librefranklin-bold" data-aos="zoon-in-up">0 <span class="moneda">COP</span></p>
                                </div>
                                <!--.card-header-->
                                <div class="card-body">
                                    <p class="card-text librefranklin-regular text-white" data-aos="zoon-in-left">Última marcación <span class="d-block librefranklin-bold text-white">04-03-2019 06:08 PM</span></p>
                                </div>
                                <!--.card-body-->
                            </div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                    </div><!--.indicadores-->
                    <div class="row m-0 tarjetas">
                      <div class="col-12 col-sm-6 col-lg-12 pr-0 trimestre">
                        <div class="card rounded-0 border-0" data-aos="fade-right">
                          <div class="card-header rounded-0 bg-corn border-0">
                            <h4 class="h4 card-title text-white librefranklin-bold" data-aos="zoon-in-left">Resultados financieros</h4>
                          </div><!--.card-header-->
                          <div class="card-body bg-jungle-green">
                            <p class="card-text mb-0 text-white librefranklin-semibold" data-aos="zoon-in-left">Tercer trimestre <br> 2018</p>
                            <div class="image-one">
                              <img src="<%=SkinPath %>images/adorno-aside.png" srcset="<%=SkinPath %>images/adorno-aside.svg" alt="" class="img-fluid position-absolute left-0 bottom-0" data-aos="zoon-in-left">
                            </div>
                            <div class="image-two">
                              <img src="<%=SkinPath %>images/oro_1.png" srcset="<%=SkinPath %>images/oro_1.svg" alt="" class="img-fluid position-absolute img-oro" width="224.152" height="168" data-aos="fade-left">
                            </div>
                          </div><!--.card-body-->
                        </div><!--.card-->
                      </div><!--.col-12-->
                      <div class="col-12 col-sm-6 col-lg-12 pr-0 boletines">
                        <div class="card rounded-0 border-0" data-aos="fade-right">
                          <div class="card-body">
                            <p class="card-text mb-0 text-white librefranklin-regular ml-auto" data-aos="zoon-in-left">Conoce nuestros</p>
                            <h3 class="h4 text-white librefranklin-bold mb-0 card-title ml-auto" data-aos="zoon-in-left">Boletines de prensa</h3>
                          </div><!--.card-body-->
                          <div class="card-footer bg-mineral-green rounded-0 d-flex justify-content-center">
                            <a href="#" class="btn btn-link text-decoration-none text-white librefranklin-regular p-0 d-flex hvr-icon-wobble-horizontal">
                              Ver boletines <span class="arrow-right hvr-icon"><img src="<%=SkinPath %>images/arrow-right.png" srcset="<%=SkinPath %>images/arrow-right.svg" alt="" class="img-fluid" width="53.793" height="26.074"></span>
                            </a>
                          </div><!--.card-footer-->
                        </div><!--.card-->
                      </div><!--.col-12-->
                    </div><!--.tarjetas-->
                    <div class="row m-0 sucribir">
                      <div class="col-12 col-sm-9 col-lg-12 m-auto pr-0">
                        <div class="card rounded-0 bg-jungle-green" data-aos="fade-up-right">
                          <div class="card-body rounded-0 text-center">
                            <div data-icon="a" class="icon" style="color: #fff;"></div>
                            <h3 class="h4 mb-0 text-center text-white librefranklin-bold" data-aos="fade-up-left">Suscríbase aquí</h3>
                            <p class="h4 mb-0 card-text text-white text-center librefranklin-regular" data-aos="fade-up-right">para recibir información corporativa de interés en su correo electrónico.</p>
                          <div class="card-footer border-0 rounded-0 bg-transparent">
                            <form class="form-suscriber">
                              <div class="form-group">
                                <label for="exampleInputEmail1" class="sr-only" data-aos="fade-right">Email address</label>
                                <input type="email" class="form-control rounded-0" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Inscríbete" data-aos="fade-right">
                              </div>
                              <button type="submit" class="btn btn-link text-decoration-none librefranklin-bold text-white d-none">Submit</button>
                            </form>
                          </div><!--.card-footer-->
                          </div><!--.card-body-->
                        </div><!--.card-->
                      </div><!--.col-12-->
                    </div><!--.sucribir-->
                </div>
                <!--.aside-->
            </div>
            <!--.row-->
        </div>
        <!--.container-->
    </section>
    <!--.section-informacion-->	

</body>